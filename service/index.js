const TableName = process.env.TABLE_NAME;
const appId = parseInt(process.env.APP_ID);
// You'll need to call dynamoClient methods to envoke CRUD operations on the DynamoDB table
const dynamoClient = require("../db");
// const UpdateCommand = require("@aws-sdk/lib-dynamodb");

// The apps routes will uses these service methods to interact with our DynamoDB Table
module.exports = class ListAppService {
  generateParams = () => {
    return {
      TableName,
      Key: { id: appId },
    };
  };

  async getListData() {
    try {
      const listData = await dynamoClient
        .get(this.generateParams())
        .promise();
      return listData.Item;
    } catch (error) {
      return error;
    }
  }

  async getTitle() {
    try {
      const listData = await dynamoClient
        .get(this.generateParams())
        .promise();
      return listData.Item.title;
    } catch (error) {
      return error;
    }
  }

  async changeTitle(title) {
    try {
      let params = this.generateParams();
      params.UpdateExpression = 'set #a = :x';
      params.ExpressionAttributeNames = { '#a': 'title' };
      params.ExpressionAttributeValues = { ':x': title };

      await dynamoClient
        .update(params)
        .promise();

      const returnValue = await dynamoClient
        .get(this.generateParams())
        .promise();
      return returnValue.Item.title;

    } catch (error) {
      return error;
    }
  }

  async getList() {
    try {
      const listData = await dynamoClient
        .get(this.generateParams())
        .promise();
      return listData.Item.items;
    } catch (error) {
      return error;
    }
  }

  async addToList(item) {
    try {
      let params = this.generateParams();
      params.UpdateExpression = 'set #a = list_append(#a, :x)';
      params.ExpressionAttributeNames = { '#a': 'items' };
      params.ExpressionAttributeValues = { ':x': [item] };

      await dynamoClient
        .update(params)
        .promise();

      const returnValue = await dynamoClient
        .get(this.generateParams())
        .promise();
      return returnValue.Item.items;

    } catch (error) {
      return error;
    }
  }

  async updateItem(index, name) {
    try {
      let params = this.generateParams();
      let newData = { name: name };
      params.UpdateExpression = "set #a[" + index + "] = :x";
      params.ExpressionAttributeNames = { '#a': 'items' };
      params.ExpressionAttributeValues = { ':x': newData };

      await dynamoClient
        .update(params)
        .promise();

      const returnValue = await dynamoClient
        .get(this.generateParams())
        .promise();
      return returnValue.Item.items;

    } catch (error) {
      return error;
    }
  }

  async deleteItem(index) {
    try {
      let params = this.generateParams();
      params.UpdateExpression = "remove #a[" + index + "]";
      params.ExpressionAttributeNames = { '#a': 'items' };

      await dynamoClient
        .update(params)
        .promise();

      const returnValue = await dynamoClient
        .get(this.generateParams())
        .promise();
      return returnValue.Item.items;

    } catch (error) {
      return error;
    }
  }
};
